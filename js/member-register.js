
$(function() {
	$('#birtdate').datepicker({
 		dateFormat: 'dd/mm/yy'
	});
});

$("#register").click(function(){
	data = {
		Name : $("#name").val(),
		Email : $("#email").val(),
		Contact_No : $("#contactNo").val(),
		Birthdate : $("#birtdate").val(),
		Gender :$('input[name=gender]:checked').val(),
		Guardian_Name : $("#gurdianName").val(),
		Guardian_Contact_No : $("#gurdianNo").val()
	}

	console.log('Member Registration : ', data);


	$.ajax({
		url: '/member',
		method: 'POST',
		data: JSON.stringify(data),
		processData: false,
		dataType: 'json',
		success: function(response) {
			if (response){
				swal(
				"Candidate Register Successfully",
				"", 
				'success'
				);
			}
			
			console.log("Inside Success");
			console.log(response);
		},
		failure: function(response) {
			console.log("Inside Failure");
			console.log(response);
			swal(
				"Oops ...",
				"Something Went Wrong. Please try again later.", 
				'error'
			);
		}
	});
});