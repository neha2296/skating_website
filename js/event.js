 


$(function() {
	$('#eventDate').datepicker({
 		dateFormat: 'dd/mm/yy'
	});

	$('input[name="daterange"]').daterangepicker({
		locale: {
      		format: 'DD/MM/YYYY , hh:mm'
    	}
	});
});

$("#save").click(function(){

	data = {
		event_name : $("#eventName").val(),
		event_date : $("#eventDate").val(),
		event_venue : $("#eventVenue").val(),
		registrationDate : $("#registrationDate").val()
	}
	console.log('Event: ', data);

	$.ajax({
		url: '/event',
		method: 'POST',
		data: JSON.stringify(data),
		processData: false,
		dataType: 'json',
		success: function(response) {
			if (response){
				swal(
				"Event Information save Successfully",
				"", 
				'success'
				);
			}
			
			console.log("Inside Success");
			console.log(response);
		},
		failure: function(response) {
			console.log("Inside Failure");
			console.log(response);
			swal(
				"Oops ...",
				"Something Went Wrong. Please try again later.", 
				'error'
			);
		}
	});
		
});


