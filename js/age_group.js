  //add age group button
$("#add").click(function() {
	 data = {
		"Age_Group" : $("#age").val(),
		"Gender" :$('input[name=gender]:checked').val(),
		"Type" :$('input[name=type]:checked').val(),
	 }

	 console.log(data)
});

//click radio button change color of table row
$('input[name="reqradio"]').change(function () {
		var parentTr = $(this).closest('tr');
		parentTr.siblings().css('background-color', '#f5f6fa');
		parentTr.css('background-color', 'rgba(46, 213, 115,0.9)');
});

//select radio button ,click next btn row value in local storage
$(document).ready(function () {
	$('#reqtable tr').click(function () {
		$('#reqtable tr').removeClass("active");
		$(this).addClass("active").find('input[name="reqradio"]').prop('checked', true);
	});

	$("#next").click(function () {
		var $ele = $('input[name="reqradio"]:checked');
		
		if ($ele.length) {
			var $tds = $ele.closest('tr').find('td');
			// var age_group = $tds.eq(1).text();
			// var gender = $tds.eq(2).text();
			// var type = $tds.eq(3).text();
			
			// console.log(age_group);            
			// console.log(gender);
			// console.log(type);		
		}
		else {
			alert("Select a row first");
		}

		localStorage.setItem("tabledata", JSON.stringify({
			"Age_Group":$tds.eq(1).text(),
			"Gender":$tds.eq(2).text(),
			"Type":$tds.eq(3).text()
			}));
		var rowdata = localStorage.getItem("tabledata");
		console.log(rowdata);
	});
});

//click row go to candidate_age_info.html (list of candidate , age group wise)
$(document).ready(function ()
{
	$("#reqtable tbody").on('click','tr td:not(:first-child)', function () {
			window.location.href="../html/candidate_age_info.html";
			console.log("Success");
	});
	
});