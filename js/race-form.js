$("#save").click(function(){

	data = {
			Race_Name : $("#raceName").val(),
			Type :$('input[name=Type]:checked').val(),
		}

		console.log('Race: ', data);

		$.ajax({
		url: '/race',
		method: 'POST',
		data: JSON.stringify(data),
		processData: false,
		dataType: 'json',
		success: function(response) {
			if (response){
				swal(
				"Race form save Successfully",
				"", 
				'success'
				);
			}
			
			console.log("Inside Success");
			console.log(response);
		},
		failure: function(response) {
			console.log("Inside Failure");
			console.log(response);
			swal(
				"Oops ...",
				"Something Went Wrong. Please try again later.", 
				'error'
			);
		}
	});
		

});